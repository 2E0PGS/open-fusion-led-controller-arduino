# TODO

* LED Pong one end of strip to the other. (Pong).
* Fade center outwards (FadeCenter).
* Fade left to right (FadeSide).
* Snake effect. Fill with colour like a progress bar and then drain.
* Add a dim trail to nightRiderSmart function.
* Add averaging to autoLDR.
* Fade3 could use local variables or find a way to fix it's slow ramp down to global brightness.
