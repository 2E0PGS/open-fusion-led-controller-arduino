/*
###################################################
Open Fusion LED Controller Project.
Designed with IRFZ44N MOSFET's / Power Transistors.
Code written by: Peter Stevenson (2E0PGS).
Forked from my older LED controller program.
Intended for use with Arduino Nano / Uno.
Main Repository: https://bitbucket.org/2E0PGS/open-fusion-led-controller-main
Arduino Repository: https://bitbucket.org/2E0PGS/open-fusion-led-controller-arduino
###################################################

Copyright (C) <2016-2019>  <Peter Stevenson> (2E0PGS)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

// Libraries.
#include <Adafruit_NeoPixel.h> // Lib for SMART led pixels.

// Input and output pins.
int dumbRedPin = 3; // Digital PWM pin 3 for red channel dumb LED strip.
int dumbGreenPin = 5;
int dumbBluePin = 6;

int smartLedPin = 10; // Data output pin for SMART LED strips.

int redAnalogSensorPin = 1; // Red analog input pin. Used for passThrough function.
int greenAnalogSensorPin = 2;
int blueAnalogSensorPin = 3;

int ldrPin = 0; // LDR (light dependant resistor) input pin.

// Global variables.
int numPixels = 249; // Number of pixels on the SMART LED string.
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(numPixels, smartLedPin, NEO_GRB + NEO_KHZ800);

boolean powerState = 1; // Track if lights should be off or on.

int timer = 100; // Timer used for delays in subroutines.

int debugLevel = 0; // Used for setting debugging level.
int mode = 1; // The variable that controls the mode we are in.
int ledOutputChoice = 0; //variable for choosing SMART of Dumb LED output.

int brightness = 255; // Master brightness for overall effect brightness.

int redBrightness = 0; // Custom colour global.
int greenBrightness = 0;
int blueBrightness = 0;

bool zeroDelayNeeded = 0; // Track if we need a 0 delay for instantly changing mode.

String inputString = ""; // String to hold incoming USB serial data. Must be global due to interpolling of CPU.
bool stringComplete = false;  // Whether the USB serial string is complete. Must be global due to interpolling of CPU.

int previousRedValue = 0; // Store the value previously assigned to lights. Used for abstracting updates to lights.
int previousGreenValue = 0;
int previousBlueValue = 0;

void setup() {
  // Not sure if pin mode is needed. It maybe automatic now.
  pinMode(dumbRedPin, OUTPUT); // Red channel is pin 3.
  pinMode(dumbGreenPin, OUTPUT); // Green channel is pin 5.
  pinMode(dumbBluePin, OUTPUT); // Blue channel is pin 6.
  
  Serial.begin(9600); // Start serial connection at 9600 baud rate.
  pixels.begin(); // Initialize the NeoPixel library.
  clearLeds(); // Ensure LEDs are clear on start up.
}

void loop() { // Which mode do we want to run? Check serial. This is the master loop.
  checkSerial();
  checkMode();
}

void delayFunction() { // We create a custom delay function so we can poll stuff during otherwise dead time.
  int inputDelayTime = timer;
  if (debugLevel >= 2) {
    Serial.println("inputDelayTime: " + String(inputDelayTime));
  }
  while (inputDelayTime >= 1) {
    inputDelayTime--;
    delay(1);
    checkSerial(); // Check serial for inputs during delay periods.
    if (zeroDelayNeeded == 1) {
      inputDelayTime = 0;
      return;
    }
  }
}

void jump3() { // Jump 3 sequence. RED then GREEN then BLUE.
  zeroDelayNeeded = 0;
  setLedRgbValues(brightness, 0, 0);
  delayFunction();
  if (zeroDelayNeeded == 1) { // Check if we need to exit.
    return; // Exit.
  }              
  
  setLedRgbValues(0, brightness, 0);
  delayFunction();
  if (zeroDelayNeeded == 1) { // Check if we need to exit.
    return; // Exit.
  }               
   
  setLedRgbValues(0, 0, brightness);
  delayFunction();
  if (zeroDelayNeeded == 1) { // Check if we need to exit.
    return; // Exit.
  }               
}

void jump7() { // Jump 7 sequence. RED then GREEN then BLUE then YELLOW then PURPLE then TURQUOISE then WHITE.
  zeroDelayNeeded = 0;
  setLedRgbValues(brightness, 0, 0);
  delayFunction();
  if (zeroDelayNeeded == 1) { // Check if we need to exit.
    return; // Exit.
  }              
    
  setLedRgbValues(0, brightness, 0);
  delayFunction();
  if (zeroDelayNeeded == 1) { // Check if we need to exit.
    return; // Exit.
  }                  
  
  setLedRgbValues(0, 0, brightness);
  delayFunction();
  if (zeroDelayNeeded == 1) { // Check if we need to exit.
    return; // Exit.
  }
  
  setLedRgbValues(brightness, brightness, 0);
  delayFunction();
  if (zeroDelayNeeded == 1) { // Check if we need to exit.
    return; // Exit.
  }
  
  setLedRgbValues(brightness, 0, brightness);
  delayFunction();
  if (zeroDelayNeeded == 1) { // Check if we need to exit.
    return; // Exit.
  }
  
  setLedRgbValues(0, brightness, brightness);
  delayFunction();
  if (zeroDelayNeeded == 1) { // Check if we need to exit.
    return; // Exit.
  }
  
  setLedRgbValues(brightness, brightness, brightness);
  delayFunction();
  if (zeroDelayNeeded == 1) { // Check if we need to exit.
    return; // Exit.
  }
  
  setLedRgbValues(0, 0, 0);
}

void checkMode() {
  if (zeroDelayNeeded == 1) {
    clearLeds();
  }
  if (powerState == 1) {
    if (mode == 1){
      autoLdr();
    }
    else if (mode == 2) {
      jump3();
    }
    else if (mode == 3) {
      jump7();
    }
    else if (mode == 4) {
      fade3();
    }
    else if (mode == 5) {
      fadeCustomColour();
    }
    else  if (mode == 10) {
      passThrough();
    }
    else if (mode == 11 && ledOutputChoice == 1) {
      randomDropletSmart();
    }
    else if (mode == 12 && ledOutputChoice == 1) {
      nightRiderSmart();
    }
    else if (mode == 13) {
      customColour();
    }
  }
  else {
    // If power state is 0 run no code.
    clearLeds(); // Clear all the LEDs down.
  }
  if (debugLevel >= 1) {
    Serial.println("mode: " + String(mode));
  }
}

void checkSerial() { // Sub for checking serial input.
  while (Serial.available()) {
    char inChar = (char)Serial.read();
    if (inChar == '\n') {      
      
      if (debugLevel >= 1) {
        Serial.println("inputString: " + inputString);
      }

      int commaIndex = inputString.indexOf(',');
      int commaIndex2 = inputString.indexOf(',', commaIndex + 1);
      int commaIndex3 = inputString.indexOf(',', commaIndex2 + 1);
      String command = inputString.substring(0, commaIndex);
      String value1 = inputString.substring(commaIndex + 2, commaIndex2);
      String value2 = inputString.substring(commaIndex2 + 2, commaIndex3);
      String value3 = inputString.substring(commaIndex3 + 2, inputString.length());

      if (debugLevel >= 1) {
        Serial.println("command: " + command);
        Serial.println("value1: " + value1);
        Serial.println("value2: " + value2);
        Serial.println("value3: " + value3);
      }

      bool commandFound = true;
      
      if (command == "setpower") {
        powerState = value1.toInt();
      }
      else if(command == "setoutput") {
        clearLeds();
        ledOutputChoice = value1.toInt();        
      }
      else if(command == "setmode") {
        mode = value1.toInt();
      }
      else if(command == "settimer") {
        timer = value1.toInt();
      }
      else if(command == "setdebug") {
        debugLevel = value1.toInt();
      }
      else if(command == "setbrightness") {
        brightness = value1.toInt();
      }
      else if(command == "setcustom") {
        redBrightness = value1.toInt();
        greenBrightness = value2.toInt();
        blueBrightness = value3.toInt();
      }
      else if(command == "setnumpixels") {
        //pixels.updateLength(value1.toInt());
        //numPixels = value1.toInt();
      }
      else {
        commandFound = false;        
      }

      if (commandFound == true) {
        Serial.println("OK!");
      }
      else{
        Serial.println("ERROR! Command not recognised.");
      }
            
      inputString = ""; // Clear the input string ready for next full command.
      zeroDelayNeeded = 1; // Ensure we zero delay methods to quit fast and change to new mode.
    }
    else {
      inputString += inChar;
    }

  }
}

void randomDropletSmart() {
  zeroDelayNeeded = 0;
  int randomPixel = random(numPixels); // Generate a random number between 0 and number of pixels we have.
  int randomColour1 = random(brightness); // Generate a random number between 0 - global brightness. Used for colour set.
  int randomColour2 = random(brightness);
  int randomColour3 = random(brightness);
  setLowLevelSmartRgbValueSingle(randomPixel, randomColour1, randomColour2, randomColour3);
  delayFunction(); //change every 1 second.
}

void fade3() {
  zeroDelayNeeded = 0;
  while (redBrightness < brightness) {
    setLedRgbValues(redBrightness, greenBrightness, blueBrightness);
    redBrightness++;
    delayFunction();
    if (zeroDelayNeeded == 1) { // Check if we need to exit.
      return; // Exit.
    }
  }
  while (blueBrightness > 0) {
    setLedRgbValues(redBrightness, greenBrightness, blueBrightness);
    blueBrightness--;
    delayFunction();
    if (zeroDelayNeeded == 1) { // Check if we need to exit.
      return; // Exit.
    }
  }
  while (greenBrightness < brightness) {
    setLedRgbValues(redBrightness, greenBrightness, blueBrightness);
    greenBrightness++;
    delayFunction();
    if (zeroDelayNeeded == 1) { // Check if we need to exit.
      return; // Exit.
    }
  }
  while (redBrightness > 0) {
    setLedRgbValues(redBrightness, greenBrightness, blueBrightness);
    redBrightness--;
    delayFunction();
    if (zeroDelayNeeded == 1) { // Check if we need to exit.
      return; // Exit.
    }
  }
  while (blueBrightness < brightness) {
    setLedRgbValues(redBrightness, greenBrightness, blueBrightness);
    blueBrightness++;
    delayFunction();
    if (zeroDelayNeeded == 1) { // Check if we need to exit.
      return; // Exit.
    }
  }
  while (greenBrightness > 0) {
    setLedRgbValues(redBrightness, greenBrightness, blueBrightness);
    greenBrightness--;
    delayFunction();
    if (zeroDelayNeeded == 1) { // Check if we need to exit.
      return; // Exit.
    }
  }
}

void fadeCustomColour() { // Fade from 0 to global brightness and then global brightness to 0 in colour white.
  zeroDelayNeeded = 0;
  
  int redBrightnessFade = redBrightness; // Reset them for each run to start from 0.
  int greenBrightnessFade = greenBrightness;
  int blueBrightnessFade = blueBrightness;
  
  while (redBrightnessFade > 0 || greenBrightnessFade > 0 || blueBrightnessFade > 0) { 
    setLedRgbValues(redBrightnessFade, greenBrightnessFade, blueBrightnessFade);
    delayFunction();
    if (zeroDelayNeeded == 1) { // Check if we need to exit.
      return; // Exit.
    }
    if (redBrightnessFade > 0) {
      redBrightnessFade--;
    }
    if (greenBrightnessFade > 0) {
      greenBrightnessFade--;
    }
    if (blueBrightnessFade > 0) {
      blueBrightnessFade--;
    }
  }
  while (redBrightnessFade < redBrightness || greenBrightnessFade < greenBrightness || blueBrightnessFade < blueBrightness) {
    setLedRgbValues(redBrightnessFade, greenBrightnessFade, blueBrightnessFade);
    delayFunction();
    if (zeroDelayNeeded == 1) { // Check if we need to exit.
      return; // Exit.
    }
    if (redBrightnessFade < redBrightness) {
      redBrightnessFade++;
    }
    if (greenBrightnessFade < greenBrightness) {
      greenBrightnessFade++;
    }
    if (blueBrightnessFade < blueBrightness) {
      blueBrightnessFade++;
    }
  }  
}

void customColour() {
  zeroDelayNeeded = 0;
  setLedRgbValues(redBrightness, greenBrightness, blueBrightness);
}

void passThrough() {
  zeroDelayNeeded = 0;
  int redBrightnessRead = analogRead(redAnalogSensorPin); // Read in the sensor.
  int greenBrightnessRead = analogRead(greenAnalogSensorPin);
  int blueBrightnessRead = analogRead(blueAnalogSensorPin);
  redBrightness = map(redBrightnessRead, 0, 1024, 0, 255); // Map the red input analog pin read of 0 - 1024 to a output brightness of 0 - 255.
  greenBrightness = map(greenBrightnessRead, 0, 1024, 0, 255); 
  blueBrightness = map(blueBrightnessRead, 0, 1024, 0, 255);  
  setLedRgbValues(redBrightness, greenBrightness, blueBrightness);
}

void autoLdr() {
  zeroDelayNeeded = 0;
  int ldrBrightnessRead = analogRead(ldrPin); // Read in the sensor.
  if(ldrBrightnessRead > 70) {
    brightness = map(ldrBrightnessRead, 0, 1024, 0, 255); // Maps the LDR input of 0 - 1024 to a output brightness of 0 - 255.
    setLedRgbValues(brightness, brightness, brightness);
  }
  if(ldrBrightnessRead < 70) { // If LDR reads a low brightness level then go night mode.
    setLedRgbValues(30, 10, 0);
  }
}

void nightRiderSmart() {
  zeroDelayNeeded = 0;
  clearLeds();
  // Count up the pixels.
  for(int i=0;i<numPixels;i++) {
    setLowLevelSmartRgbValueSingle(i, redBrightness, greenBrightness, blueBrightness);
    delayFunction();
    if (zeroDelayNeeded == 1) { // Check if we need to exit.
      return; // Exit.
    }   
    if (i >= 1) {
      setLowLevelSmartRgbValueSingle((i - 1), 0, 0, 0);
      delayFunction();
      if (zeroDelayNeeded == 1) { // Check if we need to exit.
        return; // Exit.
      }   
    }
  }
  // Count back down the pixels.
  for(int i=numPixels;i>=0;i--) {
    setLowLevelSmartRgbValueSingle(i, redBrightness, greenBrightness, blueBrightness);
    delayFunction();
    if (zeroDelayNeeded == 1) { // Check if we need to exit.
      return; // Exit.
    }
    if (i <= numPixels) {
      setLowLevelSmartRgbValueSingle((i + 1), 0, 0, 0);
      delayFunction();
      if (zeroDelayNeeded == 1) { // Check if we need to exit.
        return; // Exit.
      }   
    }
  }
}

void clearLeds() {
  // Clear all the LEDs down.
  setLowLevelSmartRgbValueFill(0, 0, 0);
  setLowLevelDumbRgbValueFill(0, 0, 0);
  previousRedValue = 0;
  previousGreenValue = 0;
  previousBlueValue = 0;
}

void setLedRgbValues(int r, int g, int b) {
  if (debugLevel >= 2) {
    Serial.println("setLedRgbValues called");
    Serial.println("r: " + String(r));
    Serial.println("g: " + String(g));
    Serial.println("b: " + String(b));
  }
  // Only update LEDs if values are new.
  if (previousRedValue != r || previousGreenValue != g || previousBlueValue != b) {
    if (debugLevel >= 2) {
      Serial.println("not equal");
      Serial.println("previousRedValue: "  + String(previousRedValue));
      Serial.println("previousGreenValue: " + String(previousGreenValue));
      Serial.println("previousBlueValue: " + String(previousBlueValue));
    }
    if (ledOutputChoice == 0) {    
      setLowLevelDumbRgbValueFill(r, g, b);
    }
    else if (ledOutputChoice == 1) {
      setLowLevelSmartRgbValueFill(r, g, b);
    }
    previousRedValue = r;
    previousGreenValue = g;
    previousBlueValue = b;
  }
}

// Try not to use low level method unless absolutely required. 
// Instead try use the more abstract method: setLedRgbValues
void setLowLevelSmartRgbValueSingle(int pixelId, int r, int g, int b) {
  pixels.setPixelColor(pixelId, pixels.Color(r,g,b));
  pixels.show();
}

void setLowLevelSmartRgbValueFill(int r, int g, int b) {
  pixels.fill(pixels.Color(r, g, b), 0, numPixels);
  pixels.show();
}

void setLowLevelDumbRgbValueFill(int r, int g, int b) {
  analogWrite(dumbRedPin, r);
  analogWrite(dumbGreenPin, g);
  analogWrite(dumbBluePin, b);
}
