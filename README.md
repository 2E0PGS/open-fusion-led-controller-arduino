# Open Fusion LED Controller Project

## This is the Core Arduino code for the project

Handles the driving and functions of the LED strips.

### Links to other repositories

* Main repository for documentation and diagrams: [open-fusion-led-controller-main](https://bitbucket.org/2E0PGS/open-fusion-led-controller-main)
* Core Arduino repository: [open-fusion-led-controller-arduino](https://bitbucket.org/2E0PGS/open-fusion-led-controller-arduino)
* Web Arduino repository: [open-fusion-led-controller-web-arduino](https://bitbucket.org/2E0PGS/open-fusion-led-controller-web-arduino)
* Raspberry Pi repository: [open-fusion-led-controller-raspberrypi](https://bitbucket.org/2E0PGS/open-fusion-led-controller-raspberrypi)
* Windows remote repository: [open-fusion-led-controller-win-remote](https://bitbucket.org/2E0PGS/open-fusion-led-controller-win-remote)
